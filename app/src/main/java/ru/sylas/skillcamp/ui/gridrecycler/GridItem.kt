package ru.sylas.skillcamp.ui.gridrecycler

data class GridItem(
    val time_text: String,
    val image: Int
)
