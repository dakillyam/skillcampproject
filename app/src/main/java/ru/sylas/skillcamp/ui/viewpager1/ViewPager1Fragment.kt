package ru.sylas.skillcamp.ui.viewpager1

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import ru.sylas.skillcamp.R

class ViewPager1Fragment : Fragment(R.layout.fragment_viewpager1) {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val viewPager: ViewPager = view.findViewById(R.id.viewpager)
        val tabs : TabLayout = view.findViewById(R.id.tabs)
        viewPager.adapter = ViewPager1PagerAdapter(parentFragmentManager)
        tabs.setupWithViewPager(viewPager)
    }
}