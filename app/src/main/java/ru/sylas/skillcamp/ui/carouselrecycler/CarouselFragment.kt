package ru.sylas.skillcamp.ui.carouselrecycler

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import ru.sylas.skillcamp.R

class CarouselFragment :Fragment(R.layout.fragment_carousel){
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val viewPager2 :ViewPager2 = view.findViewById(R.id.viewpager2)
        val tabs :  TabLayout = view.findViewById(R.id.carousel_tabs)
        viewPager2.adapter = CarouselRecyclerView()
        TabLayoutMediator(tabs,viewPager2){_,_->}.attach()
    }
}