package ru.sylas.skillcamp.ui.gridrecycler

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import ru.sylas.skillcamp.R

class GridRecyclerFragment : Fragment(R.layout.fragment_grid_recycler) {


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val list: List<GridItem> = listOf(
            GridItem("11:00",R.drawable.imageone),
            GridItem("13:00",R.drawable.imagetwo),
            GridItem("16:00",R.drawable.imageone),
            GridItem("11:30",R.drawable.imagetwo),
            GridItem("11:00",R.drawable.imageone),
        )
        val recyclerView:RecyclerView = view.findViewById(R.id.recyclerView)
        recyclerView.adapter = GridRecyclerAdapter(requireContext(),list)

    }
}